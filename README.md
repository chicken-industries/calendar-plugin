# Calendar Plugin

This UE4 plugin can be used to add a simulated calendar. The **calendar** class takes real time seconds (a tick function) and converts them into minutes, hours, days, seasons and years (called GameTime). This calendar class can by used by the **DynamicSunlight** Blueprint, which works similiar to the **SunSky Blueprint** from the official **Sun Position Calculator** plugin. 
The calendar data can is used to simulate the sun light rotation of a directional light, for specific planetoidal ecliptic and lattitude of the observers position.

The main difference to the official plugin is, that you can specifiy `days per season` and `real time seconds per in game hours`. The latter specifies how fast the simulation of the game calendar should happen. Days per season can be specified to personalize the calendar to the needs of your game. The other conversions are fixed at the moment.

* There are four seasons per year (spring, summer, autumn, winter)
* 24 hours per days
* 60 minutes per hour

In game seconds, milliseconds... are left out, it seems to small to be interesting on a time scale of years. Also months are left out, because their irregularity seems unattractive for simple game calendars. Correctly a month has to be defined by a naturale sattellite (moon). This could be a future feature. 

## Dependencies

* [ChickenMathLibrary](https://gitlab.com/chicken-industries/ChickenMathLibrary)

## Usage

The calendar class is wrapped by a actor component (CalendarComponent), which is meant to be attached to a GameState. The DynamicSunlight Blueprint and other custom classes and blueprints can access the calendar data from the GameState. The most basic functions are
```C++
UFUNCTION(BlueprintCallable)
void SetCurrentGameTime(FGameTime const& GameTime);

UFUNCTION(BlueprintPure)
FGameTime GetCurrentGameTime() const;
```

To use the dynamic sunlight, simply place it into the level. Specify ecplictic and lattitude. You can turn on the earth and sun models to visualize how the sun light rotation gets simulated.

![](Resources/CalendarPluginExample.png)

## Future Plans

* (multiple) moon(s)


