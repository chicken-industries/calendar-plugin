// Copyright 2024 Sebastian Hennemann All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Fixed/Fixed64/Fixed64.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "CalendarSubsystem.generated.h"


USTRUCT(BlueprintType)
struct CALENDARPLUGIN_API FCalendarTime
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Year = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Day = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Hour = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Minute = 0;
	
	// 24 * 60 * 60 = 86400 would mean real time.
	static constexpr FFixed64 RealTimeSecondsPerInGameDay = 600;
	static constexpr int MinutesPerHour = 60;
	static constexpr int HoursPerDay = 24;
	static constexpr int DaysPerYear = 15;

	static constexpr FFixed64 RealTimeSecondsPerInGameHour = RealTimeSecondsPerInGameDay / HoursPerDay;

	static constexpr FCalendarTime ConstructFromRealTime(FFixed64 RealTimeSeconds)
	{
		FFixed64 YearFullDecimal = ConvertToYearDecimal(RealTimeSeconds);
		FFixed64 Year = FFixed64::Floor(YearFullDecimal);
		FFixed64 YearPartial = YearFullDecimal - Year;

		FFixed64 DayFullDecimal = YearPartial * DaysPerYear;
		FFixed64 Day = FFixed64::Floor(DayFullDecimal);
		FFixed64 DayPartial = DayFullDecimal - Day;

		FFixed64 HourFullDecimal = DayPartial * HoursPerDay;
		FFixed64 Hour = FFixed64::Floor(HourFullDecimal);
		FFixed64 HourPartial = HourFullDecimal - Hour;

		FFixed64 Minute = FFixed64::Floor(HourPartial * MinutesPerHour);

		return FCalendarTime{static_cast<int>(Year),
			static_cast<int>(Day),
			static_cast<int>(Hour),
			static_cast<int>(Minute)};
	}

	constexpr FFixed64 ConvertToRealTimeSeconds() const
	{
		FFixed64 CalendarTimeInDays = 0;
		CalendarTimeInDays += Year * DaysPerYear;
		CalendarTimeInDays += Day;
		CalendarTimeInDays += FFixed64(Hour) / HoursPerDay;
		CalendarTimeInDays += FFixed64(Minute) / (HoursPerDay * MinutesPerHour);
	
		return RealTimeSecondsPerInGameDay * CalendarTimeInDays;
	}

	static constexpr FFixed64 ConvertToRealTimeSeconds(const FCalendarTime& CalendarTime)
	{
		return CalendarTime.ConvertToRealTimeSeconds();
	}

	static constexpr FFixed64 ConvertToYearDecimal(FFixed64 RealTimeSeconds)
	{
		return RealTimeSeconds / (RealTimeSecondsPerInGameDay * DaysPerYear);
	}

	constexpr FFixed64 ConvertToYearDecimal() const
	{
		return ConvertToYearDecimal(ConvertToRealTimeSeconds());
	}
	
	FString GetDateAsString() const 
	{
		return FString::Printf(TEXT("Year: %i, Day: %i"), Year, Day);
	}

	FString GetTimeAsString() const
	{
		return FString::Printf(TEXT("%02i:%02i"), Hour, Minute);
	}

	FString GetDateAndTimeAsString() const
	{
		return GetDateAsString() + ", " + GetTimeAsString();
	}
};

constexpr FCalendarTime operator-(FCalendarTime const& A, FCalendarTime const& B)
{
	FFixed64 A_RealTime = A.ConvertToRealTimeSeconds();
	FFixed64 B_RealTime = B.ConvertToRealTimeSeconds();

	return FCalendarTime::ConstructFromRealTime(FMath::Abs(A_RealTime - B_RealTime));
}

UCLASS()
class CALENDARPLUGIN_API UCalendarTimeLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	// Returns a FString with Year and Day. First day/year has value 0.
	UFUNCTION(BlueprintPure)
	static FString GetDateAsString(FCalendarTime const& CalendarTime) { return CalendarTime.GetDateAsString(); }

	// Returns a FString as HH:MM
	UFUNCTION(BlueprintPure)
	static FString GetTimeAsString(FCalendarTime const& CalendarTime) { return CalendarTime.GetTimeAsString(); }

	// Returns a FString containing all members of the CalendarTime struct
	UFUNCTION(BlueprintPure)
	static FString GetCalendarTimeAsString(FCalendarTime const& CalendarTime) { return CalendarTime.GetDateAndTimeAsString(); }
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHourChange, int, NewHour);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDayChange, int, NewDay);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnYearChange, int, NewYear);


UCLASS(ClassGroup = (Custom))
class CALENDARPLUGIN_API UCalendarSubsystem : public UWorldSubsystem
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FOnHourChange OnHourChange;
	
	UPROPERTY(BlueprintAssignable)
	FOnDayChange OnDayChange;

	UPROPERTY(BlueprintAssignable)
	FOnYearChange OnYearChange;
	
private:
	FFixed64 CurrentTimeInRealSeconds = 0;
	
public:
	UFUNCTION(BlueprintCallable)
	void AdvanceTime(FFixed64 DeltaSeconds);
	
	UFUNCTION(BlueprintCallable)
	void SetCurrentCalendarTime(FCalendarTime const& CalendarTime);

	UFUNCTION(BlueprintCallable)
	void SetCurrentCalendarTimeFromRealTime(FFixed64 RealTimeSeconds);

	// Should be used when loading a game, so we don't call OnChange Delegates
	UFUNCTION(BlueprintCallable)
	void SetCurrentCalendarTimeFromRealTimeWithoutDelegates(FFixed64 RealTimeSeconds);
	 
	UFUNCTION(BlueprintPure)
	FCalendarTime GetCurrentCalendarTime() const;

	// Similar calculations to CalcCalendarTime(). But returns the progress of the current year/day. This is fed into routines that calculate planet rotations.
	UFUNCTION(BlueprintPure)
	void GetCurrentCalendarTimeDecimals(FFixed64& YearPartial, FFixed64& DayPartial) const;

private:
	void CallCalendarTimeChangedDelegates(const FCalendarTime& OldCalendarTime, const FCalendarTime& NewCalendarTime) const;
};
