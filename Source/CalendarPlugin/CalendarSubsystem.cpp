// Copyright 2024 Sebastian Hennemann All rights reserved

#include "CalendarSubsystem.h"


void UCalendarSubsystem::AdvanceTime(FFixed64 DeltaSeconds)
{
	SetCurrentCalendarTimeFromRealTime(CurrentTimeInRealSeconds + DeltaSeconds);
}

void UCalendarSubsystem::SetCurrentCalendarTimeFromRealTime(FFixed64 RealTimeSeconds)
{
	FCalendarTime OldCalendarTime = GetCurrentCalendarTime();
	
	CurrentTimeInRealSeconds = RealTimeSeconds;

	FCalendarTime NewCalendarTime = GetCurrentCalendarTime();

	CallCalendarTimeChangedDelegates(OldCalendarTime, NewCalendarTime);
}

void UCalendarSubsystem::SetCurrentCalendarTimeFromRealTimeWithoutDelegates(FFixed64 RealTimeSeconds)
{
	CurrentTimeInRealSeconds = RealTimeSeconds;
}

void UCalendarSubsystem::SetCurrentCalendarTime(FCalendarTime const& CalendarTime)
{
	SetCurrentCalendarTimeFromRealTime(CalendarTime.ConvertToRealTimeSeconds());
}

void UCalendarSubsystem::GetCurrentCalendarTimeDecimals(FFixed64& YearPartial, FFixed64& DayPartial) const
{
	FFixed64 YearFullDecimal = FCalendarTime::ConvertToYearDecimal(CurrentTimeInRealSeconds);
	YearPartial = YearFullDecimal - FFixed64::Floor(YearFullDecimal);

	FFixed64 DayFullDecimal = YearPartial * FCalendarTime::DaysPerYear;
	DayPartial = DayFullDecimal - FFixed64::Floor(DayFullDecimal);
}

FCalendarTime UCalendarSubsystem::GetCurrentCalendarTime() const
{
	return FCalendarTime::ConstructFromRealTime(CurrentTimeInRealSeconds);
}

void UCalendarSubsystem::CallCalendarTimeChangedDelegates(const FCalendarTime& OldCalendarTime, const FCalendarTime& NewCalendarTime) const
{
	if (NewCalendarTime.Year != OldCalendarTime.Year)
	{
		OnYearChange.Broadcast(NewCalendarTime.Year);
	}

	if (NewCalendarTime.Day != OldCalendarTime.Day)
	{
		OnDayChange.Broadcast(NewCalendarTime.Day);
	}

	if (NewCalendarTime.Hour != OldCalendarTime.Hour)
	{
		OnHourChange.Broadcast(NewCalendarTime.Hour);
	}
}